package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;
    public static void main(String[] args) {
        ///// Oppgave 6
        // Have two pokemon fight until one is defeated
        pokemon1 = new Pokemon("Pikachu", 94, 12);
        pokemon2 = new Pokemon("Oddish", 100, 3);
        int turn = 0;
        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            if (turn == 0) {
                System.out.println(pokemon1.getName() + " attacks " + pokemon2.getName() + ".");
                pokemon1.attack(pokemon2);
                turn++;
            }
            else {
                System.out.println(pokemon2.getName() + " attacks " + pokemon1.getName() + ".");
                pokemon2.attack(pokemon1);
                turn--;
            }
        }
        if (pokemon1.isAlive()) {
            System.out.println(pokemon2.getName() + " is defeated by " + pokemon1.getName() + ".");
        }
        else {
            System.out.println(pokemon1.getName() + " is defeated by " + pokemon2.getName() + ".");
        }
    }
}
